####
# BUILD STAGE
####

FROM klakegg/hugo:0.87.0-alpine-onbuild AS hugo

COPY ./site /src

####
# FINAL STAGE
####

FROM alpine:3.14

ARG PUID=1000
ARG PGID=1000

RUN set -xe \
    && addgroup -g ${PGID} -S web \
    && adduser -u ${PUID} -G web -h /var/local/web -D web \
    && apk add --no-cache --purge -uU \
        ca-certificates \
    && rm -rf /var/cache/apk/* /tmp/*

ARG GIT_COMMIT
LABEL REPO="https://gitlab.com/olmax99/landing-forty"
LABEL GIT_COMMIT=$GIT_COMMIT

# Because of https://github.com/docker/docker/issues/14914
ENV PATH=$PATH:/usr/bin

COPY --from=hugo /usr/lib/hugo/hugo /usr/lib/hugo/hugo
COPY --from=hugo /etc/profile.d/hugo.sh /etc/profile.d/hugo.sh
COPY --from=hugo /bin/hugo /bin/hugo
COPY --from=hugo /bin/hugo-official /bin/hugo-official
COPY --from=hugo /src /src

WORKDIR /src

USER web

# ENTRYPOINT ["/entrypoint.sh"]

CMD ["hugo", "server"]
